const addContainer = document.querySelector('.list-container');
const unorderedList = document.querySelector('.unorderedlist');

const myForm = document.querySelector('#form');



function formSubmition(event) {
    event.preventDefault();
    const inputValue = document.querySelector('#maintext').value;

    if (inputValue === "") {
        alert("Please enter text to the input field")
    } else {
        // creating list item
        const listItem = document.createElement("li");
        listItem.textContent = inputValue;
        listItem.style.fontSize = "25pxgi";
        unorderedList.appendChild(listItem);
        // creating button for remove functinallity
        const button = document.createElement("button");
        button.innerText = "remove";
        button.classList.add("removeButton");
        unorderedList.appendChild(button);
       
        document.querySelector('#maintext').value = "";


        function removeItem() {

            listItem.remove();
            button.remove();
        }
    
        button.addEventListener('click', removeItem);

    }


   
};




myForm.addEventListener('submit', formSubmition);

